// Test af Java 11's nye faciliteter. 
// Version2.dk 2018.

import java.net.*;
import java.net.http.*;
import java.util.concurrent.*;

public class HttpclientTest {

	public static void main(String... args) throws Exception {
		var client = HttpClient
		.newBuilder()
		.version(HttpClient.Version.HTTP_2)
		.build();
		
		var request = HttpRequest
		.newBuilder()
		.uri(URI.create("https://http2.akamai.com/demo"))
		.build();
		
		var response = client.send(request, HttpResponse.BodyHandlers.ofString());
		System.out.println("Fik HTTP-kode tilbage: " + response.statusCode());
		assert response.body().contains("Your browser supports HTTP/2!");
		
		var listener = new WebSocket.Listener() {
        	public CompletionStage<?> onText(WebSocket webSocket,
                                         CharSequence message,
                                         boolean last) {
				System.out.println("Websocket-serveren svarede: " + message);
				assert message.equals("Hvad drikker Møller?");
				return null;
			}
        };  
		
   		var websocketFuture = client.newWebSocketBuilder()
           .buildAsync(URI.create("wss://echo.websocket.org"), listener);
		
		websocketFuture.join().sendText("Hvad drikker Møller?", true);
		
		// Vi venter lige på svar fra serveren, før vi slutter...
		Thread.sleep(10000);
	}
}